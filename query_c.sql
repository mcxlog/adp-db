SELECT distinct(m.director)
FROM movie as m, (actor NATURAL JOIN person) as a, person as p
WHERE m.title = a.movie_title and p.name = m.director and p.country = ALL
(
  SELECT a1.country
  FROM movie as m1, (actor NATURAL JOIN person) as a1
  WHERE m1.title = a1.movie_title and m1.release_year = a1.movie_year
        and m.director = m1.director
);