SELECT distinct(name) 
FROM composer NATURAL JOIN soundtrack
WHERE soundtrack.title IN (SELECT title
FROM track NATURAL JOIN soundtrack
GROUP BY title
HAVING sum(track_length) >= ALL(
      SELECT sum(track_length)
      FROM track NATURAL JOIN soundtrack
      GROUP BY title));