drop table if exists person       cascade;
drop table if exists book         cascade;
drop table if exists movie        cascade;
drop table if exists soundtrack   cascade;
drop table if exists track        cascade;
drop table if exists author       cascade;
drop table if exists actor        cascade;
drop table if exists composer     cascade;

create table person
   (name        varchar(255) not null unique,
    birthdate   date         not null,
    city        varchar(255) not null,
    country     varchar(255) not null,
    
    primary key(name));

create table book
   (isbn            varchar(32) not null unique,
    title           varchar(255) not null,
    publisher       varchar(255) not null,
    release_date    date         not null, 
    
    primary key(isbn));

create table movie
   (title           varchar(255)    not null,
    release_year    integer         not null,
    movie_length    integer         not null,
    isbn            varchar(255)    not null,
    director        varchar(255)    not null,
    
    primary key(title, release_year),
    foreign key(isbn) references book(isbn));

create table soundtrack
   (title         varchar(255)  not null unique,
    label         varchar(255)  not null,
    release_date  date          not null,
    movie_title   varchar(255)  not null,
    movie_year    integer       not null,
    
    primary key(title),
    foreign key(movie_title, movie_year)    references movie(title, release_year));
    
create table track
   (title           varchar(255)  not null,
    track_number    integer       not null,
    track_length    time          not null,
    
    primary key(title,track_number),
    foreign key(title)    references soundtrack(title));
    

create table author
   (isbn  varchar(255) not null,
    name  varchar(255) not null,
    
    primary key(isbn, name),
    foreign key(isbn)         references book(isbn),
    foreign key(name)         references person(name));

    
create table actor
   (movie_title   varchar(255)  not null,
    movie_year    integer       not null,
    name          varchar(255)  not null,
    
    primary key(name, movie_year, movie_title),
    foreign key(movie_title,movie_year)         references movie(title,release_year),
    foreign key(name)                           references person(name));
    
create table composer
   (title    varchar(255)  not null,
    name     varchar(255)  not null,
    
    primary key(title, name),
    foreign key(title)         references soundtrack(title),
    foreign key(name)          references person(name));


-- Trigger for avoiding the insertion of a book with a release date prior
-- to the release date of the book it adapts.
create or replace function check_date_func() returns trigger 
as 
$$ 
declare book_year integer;
begin 
	SELECT extract(year from b.release_date) into book_year
	FROM book as b
	WHERE b.isbn = new.isbn;
	
	if new.release_year < book_year then
		raise exception 'Movie % could not have been released before the adapted book.', new.title; 
	end if; 
	return new;
end 
$$ 
language plpgsql;

-- Create Trigger
create trigger check_date after update or insert on movie 
for each row execute procedure check_date_func(); 