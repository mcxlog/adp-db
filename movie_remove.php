<html> 
  <body> 
  <h2>Removing movies from database: </h2> 
    <?php 
    
      $host = "db.ist.utl.pt";
      $port = 5432;
      $user = "ist167725";
      $password = "guuu4689";
      $dbname = $user;
     
      $connection = pg_connect("host=$host port=$port user=$user password=$password dbname=$dbname") or die(pg_last_error());
        
      $m_title = $_REQUEST['movie_title_inserted'];
      $m_year = $_REQUEST['movie_year_inserted'];
      $m_lenght = $_REQUEST['movie_lenght_inserted'];
      $b_isbn = $_REQUEST['book_isbn_inserted'];
      $m_director = $_REQUEST['director_inserted'];
            
      foreach ($_REQUEST as $name => $value){ 
      
        if ($name == 'movies_remove'){ 
  
          foreach($_REQUEST[$name] as $movie_key){
            
            list ($title_key, $year_key) = split (',', $movie_key);
            $safe = htmlspecialchars($title_key);
            echo("Removing Movie: $safe ($year_key): ");
			
			/*Deleting process must be atomic in order to avoid database inconsistency*/
            $result = try_query_or_die("START TRANSACTION;");
            $result = try_free_or_die($result);
			
            /*Delete actors from movie to be removed*/
            $result = try_query_or_die("DELETE FROM actor WHERE movie_title = '$title_key' and movie_year = $year_key;");
            $result = try_free_or_die($result);

            /*Query for the soundtracks that belong to the movie to be removed*/
            $result_soundtracks = try_query_or_die("SELECT * FROM soundtrack WHERE movie_title = '$title_key' and movie_year = $year_key;");
           
            /*Delete composer and tracks from each soundtrack*/
            while($row = pg_fetch_assoc($result_soundtracks)){
                
              $buf = $row['title'];  
			  
			  /*Delete to be removed soundtack tracks*/
              $result = try_query_or_die("DELETE FROM track WHERE title = '$buf';");
              $result = try_free_or_die($result);
              
              /* Delete to be removed soundtrack composer*/
              $result = try_query_or_die("DELETE FROM composer WHERE title = '$buf';");
              $result = try_free_or_die($result);

              /*Delete soundtrack*/
              $result = try_query_or_die("DELETE FROM soundtrack WHERE title = '$buf';");
              $result = try_free_or_die($result);

            }
            $result_soundtracks = try_free_or_die($result_soundtracks);
            
            /*Delete movie*/
            $result = try_query_or_die("DELETE FROM movie WHERE title = '$title_key' and release_year = $year_key;");
            $result = try_free_or_die($result);
			
			$result = try_query_or_die("COMMIT;");
            $result = try_free_or_die($result);
            
            echo(" Success.<br/>");

          }
        }
      } 
      pg_close($connection);
      
      echo("<p><a href = \"home.html\">Return to home page<a></p>");
      
      function try_free_or_die($result)
      {
          $result = pg_free_result($result);
          if($result){
          
            return $result;
            
          }else{ // display error and return link
            
            echo("<p>There was an error Freeing the query</p>");
            echo("<p>");
            echo(pg_last_error());
            echo("</p>");
            echo("<p><a href = \"home.html\">Return to home page<a></p>");
            die();
          }   
      }
      function try_query_or_die($sql)
      {
          $res = pg_query($sql);
          if($res){
          
            return $res;
            
          }else{ // display error and return link
            
            echo("<p>There was an error removing the movie:</p>");
            echo("<p>");
            echo(pg_last_error());
            echo("</p>");
            echo("<p><a href = \"home.html\">Return to home page<a></p>");
            die();
          }   
      }
    ?>
  </body> 
</html>


