drop table if exists person       cascade;
drop table if exists book         cascade;
drop table if exists movie        cascade;
drop table if exists soundtrack   cascade;
drop table if exists track        cascade;
drop table if exists author       cascade;
drop table if exists actor        cascade;
drop table if exists composer     cascade;