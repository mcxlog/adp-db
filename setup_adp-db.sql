insert into person values ('James Wallnut',     '2000-03-19', 'London',     'England');
insert into person values ('Mike Palmtree',     '1998-05-30', 'Frankfurt',  'Germany');
insert into person values ('James Greenburg',   '1920-12-01', 'Berlin',     'Germany');
insert into person values ('Mathilda Goodwill', '1915-10-03', 'Frankfurt',  'Germany');
insert into person values ('Peter Stormson',    '1945-11-05', 'Frankfurt',  'Germany');
insert into person values ('Bob Ironhand',      '1975-06-01', 'Frankfurt',  'Germany');
insert into person values ('Olaf Yocatam',      '1965-06-01', 'Frankfurt',  'Germany');

insert into book values ('85-359-0277-5', 'The Journey of Love', 'Artemis Editions', '1910-07-24');
insert into book values ('81-319-0257-2', 'Chocolate and Mint',  'Script Editions',  '1950-04-21');
insert into book values ('89-45210-79-0', 'Prisms and Princes',  'Artemis Editions', '1975-08-13');

insert into movie values ('Brown Cookies',       2010, 180 , '81-319-0257-2', 'James Wallnut'  );
insert into movie values ('Darker Cookies',      2012, 120 , '81-319-0257-2', 'Mike Palmtree'  );
insert into movie values ('Geometry',            2004, 110 , '89-45210-79-0', 'Olaf Yocatam'  );
insert into movie values ('The Journey of Love', 1995, 110 , '85-359-0277-5', 'James Greenburg');

insert into soundtrack values ('Cooking and Baking',        'Corner-St Records',  '1980-11-29' , 'Brown Cookies',      2010);
insert into soundtrack values ('Darker Cooking and Baking', 'Cooking Records',    '1982-09-09' , 'Darker Cookies',     2012);
insert into soundtrack values ('Platonic Music',            'Corner-St Records',  '1967-04-04' , 'Geometry',           2004);
insert into soundtrack values ('Hearts and Red Apples',     'Love Records',       '1994-10-13' , 'The Journey of Love',1995);

insert into track values ('Cooking and Baking', 1,'02:03:27');
insert into track values ('Cooking and Baking', 2,'00:02:50');
insert into track values ('Cooking and Baking', 3,'00:03:46');
insert into track values ('Cooking and Baking', 4,'00:05:11');
insert into track values ('Cooking and Baking', 5,'00:01:30');
insert into track values ('Cooking and Baking', 6,'00:08:34');

insert into track values ('Darker Cooking and Baking', 1,'00:09:30');
insert into track values ('Darker Cooking and Baking', 2,'00:01:50');
insert into track values ('Darker Cooking and Baking', 3,'00:00:46');

insert into track values ('Platonic Music', 1,'00:09:30');
insert into track values ('Platonic Music', 2,'00:01:53');
insert into track values ('Platonic Music', 3,'01:30:46');
insert into track values ('Platonic Music', 4,'00:09:30');
insert into track values ('Platonic Music', 5,'00:04:10');
insert into track values ('Platonic Music', 6,'00:03:36');
insert into track values ('Platonic Music', 7,'00:02:47');
insert into track values ('Platonic Music', 8,'00:03:35');

insert into track values ('Hearts and Red Apples', 1,'01:00:30');
insert into track values ('Hearts and Red Apples', 2,'00:01:57');
insert into track values ('Hearts and Red Apples', 3,'00:02:18');
insert into track values ('Hearts and Red Apples', 4,'00:02:16');

insert into author values ('85-359-0277-5','Mathilda Goodwill');
insert into author values ('81-319-0257-2','Mathilda Goodwill');
insert into author values ('89-45210-79-0','Mike Palmtree');

insert into actor values ('Brown Cookies',       2010, 'Mathilda Goodwill')   ;
insert into actor values ('Brown Cookies',       2010, 'Peter Stormson')      ;
insert into actor values ('Brown Cookies',       2010, 'Bob Ironhand')        ;

insert into actor values ('Darker Cookies',      2012, 'Mathilda Goodwill')   ;
insert into actor values ('Darker Cookies',      2012, 'James Wallnut')       ;
insert into actor values ('Darker Cookies',      2012, 'James Greenburg')     ;

insert into actor values ('Geometry',            2004, 'Mathilda Goodwill')     ;
insert into actor values ('Geometry',            2004, 'Peter Stormson')       ;


insert into actor values ('The Journey of Love', 1995, 'Bob Ironhand')        ;
insert into actor values ('The Journey of Love', 1995, 'Mike Palmtree')       ;
insert into actor values ('The Journey of Love', 1995, 'Olaf Yocatam')       ;
                                                                              
insert into composer values ('Cooking and Baking',        'Mathilda Goodwill');
insert into composer values ('Cooking and Baking',        'Peter Stormson')   ;
insert into composer values ('Darker Cooking and Baking', 'Peter Stormson')   ;
insert into composer values ('Hearts and Red Apples',     'James Greenburg')  ;
insert into composer values ('Hearts and Red Apples',     'Peter Stormson')   ;
insert into composer values ('Platonic Music',            'Bob Ironhand')     ;
