SELECT distinct(a.name)
					FROM movie as m, (author NATURAL JOIN book) as b, actor as a
					WHERE m.isbn = b.isbn and a.movie_title = m.title and a.movie_year = m.release_year and a.name = b.name;